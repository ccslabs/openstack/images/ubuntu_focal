source "openstack" "ubuntu_focal" {
    flavor       = "m1.small"
    image_name   = "ubuntu_focal_ccslabs"
    networks     = ["a8bf8f65-5ee3-42db-998f-5d32e78a5a0b"]
    source_image = "2184347e-038f-48fc-8be6-342c2eec09a1"
    ssh_username = "ubuntu"
    use_blockstorage_volume = true
    volume_type  = "rbd"
    volume_size  = "5"
}

build {
  sources = ["source.openstack.ubuntu_focal"]

  provisioner "ansible" {
    playbook_file   = "./site.yml"
    user            = "ubuntu"
  }
}